Some first steps:

What you need
-------------
Create an Bitbuckt account
Install Visual Code
Install NodeJS with NPM
Install Git

Starting with Git
-----------------
1. Create a new folder and open the terminal here
2. Type "git init" to create an empty git repository on your local machine
3. Type "git clone https://anne_ma@bitbucket.org/anne_ma/test-project.git"
4. Open this folder with Visual Code
5. Create a new file next to this open
6. Type in the terminal "git status", hopefully you can see your new file listet there
7. Type "git add ." -> That means that you stage all changes
8. Type "git commit -m "git test"" and "git push"
9. Check "git status" again, your change should be disappeared
10. Check Bitbucket.org and see if you can find your file there.

More with Git
-------------